// ===== 1 =====
const background = function() {
    const paragraphs = document.getElementsByTagName("p");
    for (let i = 0; i < paragraphs.length; i++) {
        paragraphs[i].style.background = "#f00";
    }
};
background();

// ==== 2 ====
const elem = document.getElementById('optionsList');
console.log(elem);
const parent = elem.parentNode;
console.log(parent);
const child = elem.childNodes;
console.log(child);

const content = document.querySelector('#testParagraph');
content.innerText = 'This is a paragraph';

// ========= 4 ========
const main = document.querySelector('.main-header');
console.log(main);
for (let i = 0; i < main.children.length; i++) {
   let child = main.children[i];
   child.classList.remove(...child.classList);
   child.classList.add('new-item');
}
console.log(main.children);

// ====== 5 =======
let el = document.querySelectorAll('.section-title');
for (let i = 0; i < el.length; i++) {
   el[i].classList.remove('section-title');
}
console.log(el);